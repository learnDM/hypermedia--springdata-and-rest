package com.therealdanvega.otherproject.repository;


//import com.therealdanvega.otherproject.domain.ListogibB;
import com.therealdanvega.otherproject.domain.Listogib;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "listogib", path = "listogib")
public interface RepositoryListogib extends JpaRepository<Listogib, Long> {

}
