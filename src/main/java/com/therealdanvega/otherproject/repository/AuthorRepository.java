package com.therealdanvega.otherproject.repository;


import com.therealdanvega.otherproject.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;


public interface AuthorRepository extends JpaRepository<Author, Long>{
//        , PagingAndSortingRepository<Author, Long> {

    Author findAuthorByFirstNameEndingWith(@Param ("n") String n);

}