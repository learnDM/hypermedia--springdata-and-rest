package com.therealdanvega.otherproject.repository;


import com.therealdanvega.otherproject.domain.Lathe;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "lathe", path = "lathe")
public interface LatheRepository extends JpaRepository<Lathe, Long>, PagingAndSortingRepository<Lathe, Long>{


//    @Query("SELECT l FROM Lathe l WHERE l.model = :model")
//    Lathe findByModel(@Param("model") String model);

    @Override
    @Query("select l from Lathe l order by l.model")
    List<Lathe> findAll();

    @Query(value = "select * from lathes order by RAND() desc LIMIT 4", nativeQuery = true)
    List<Lathe> get4RandomLathes();

    @Query(value = "select * from lathes order by RAND() desc LIMIT 1", nativeQuery = true)
    Lathe getRandom1Lathe();

    @Query(value = "select l.model from Lathe l")
    List<String> getListOfModel();

    @Query(value = "select l.country from Lathe l")
    List<String> getListOfCountries();


//    select * from lathes   LIMIT 4



}
