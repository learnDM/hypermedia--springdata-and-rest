package com.therealdanvega.otherproject.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
public class LathePhoto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String photo;


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "lathe_id")
    @JsonBackReference
    private Lathe lathe;


    public LathePhoto(String photo) {
        this.photo = photo;
    }

    public LathePhoto(String photo, Lathe lathe) {
        this.photo = photo;
        this.lathe = lathe;
    }


    @Override
    public String toString() {
        return "LathePhoto{" +
                "id=" + id +
                ", photo='" + photo + '\'' +
                '}';
    }
}
