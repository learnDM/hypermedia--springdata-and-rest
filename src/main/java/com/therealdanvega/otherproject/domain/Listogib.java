package com.therealdanvega.otherproject.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@Entity
@Table(name = "listogib")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Listogib {


   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   Long id;
   String model;
   Integer x;
   Integer z;
   String brand;
   String exist;



}
