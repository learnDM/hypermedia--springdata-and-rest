package com.therealdanvega.otherproject.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//@JsonComponent
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@Table(name = "lathes")
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
//@EqualsAndHashCode(exclude = "photos")
//@FilterDef(name = "activeAccount", parameters = @ParamDef(name = "existParam", type = "string"))
//@Filter(name = "activeAccount", condition = "exist=:existParam")
public class Lathe implements Serializable {


// CREATE TABLE lathes (id int, model varchar(255), x int, z int, photo varchar(255), exist varchar(20) );
// insert into lathes values (5, "CTX alpha 500", 150, 500, "CTX_alpha_500_1.jpg", "yes");
// ALTER TABLE lathes CHANGE id id INT(10) AUTO_INCREMENT PRIMARY KEY;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String productId;
    String type;
    String typeurl;

    @Size(min = 4, message = "name of model can not be less than 4")

    String model;
    String modelurl;


    String producer;


    String producingCountry;
    String systemCNC;
    String fullSystemCNC;
    Integer year1;
    String condition1;
    String machineLocation;
    Integer axisCount;
    Integer diameterMax;
    Integer diameter;
    Integer bardiameter;
    Integer lengthMax;
    Integer x;
    Integer y;
    Integer z;
    Integer spindleSpeed;
    String spindlepower;
    Integer spindlediameter;
    String subspindle;
    Integer subspindleSpeed;
    String vdi;
    String toolsall;
    String toolslive;
    String toolsnotlive;
    String tailstock;
    String accuracy;
    String accuracyAxisC;
    String spindletime;
    String machineRuntime;
    Integer price;

    String descriptionen; // I nee add annotation @Lob // LOB or Large OBject
    String descriptionru;
    String video1;
    String video2;
    String manufacturer;

    Integer year;
    String country;

    @OneToMany(mappedBy = "lathe", cascade = CascadeType.ALL )
    @JsonManagedReference
    List<LathePhoto> photos = new ArrayList<>();
    String exist;



    public void addPhoto(LathePhoto lathePhoto) {
        photos.add(lathePhoto);
        lathePhoto.setLathe(this);
    }

    public void removePhoto(LathePhoto lathePhoto) {
        photos.remove(lathePhoto);
        lathePhoto.setLathe(null);
    }


    public Lathe(@Size(min = 3, message = "Name of model must be more then 3") String model, Integer x, Integer z, String country, List<LathePhoto> photos, @Size(min = 2, message = "Name of model must be more then 2") String exist) {
        this.model = model;
        this.x = x;
        this.z = z;
        this.country = country;
        this.photos = photos;
        this.exist = exist;
    }


//    public String getPhoto() {
//       if( photos==null)  System.out.println("--------------photos==null  !!!!!!!!!");
//       if( photos!=null)  System.out.println("--------------photos!=null  !!!!!!!!!");
////       if( photos.get(0)!=null)  System.out.println("--------------photos.get(0)!=null !!!!!!!!!");
////       if( photos.get(0)==null)  System.out.println("--------------photos.get(0)==null  !!!!!!!!!");
//        return photos.size()>0 ? photos.get(0).getPhoto() : "not exist";
//    }



    @Override
    public String toString() {
        return "Lathe{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", x=" + x +
                ", z=" + z +
                ", photos=" + photos +
                ", exist='" + exist + '\'' +
                '}';
    }
}






