package com.therealdanvega.otherproject.controller;

import com.therealdanvega.otherproject.domain.Author;
import com.therealdanvega.otherproject.repository.AuthorRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
@RestController
public class ControllerA {

    AuthorRepository authorRepository;


    @GetMapping(value = "/author_json")
    public List<Author> showLathes_json()   {
        return authorRepository.findAll();
    }


}
