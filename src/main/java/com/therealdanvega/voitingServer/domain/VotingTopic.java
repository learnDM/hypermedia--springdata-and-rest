package com.therealdanvega.voitingServer.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Entity
//@Table(name = "")
public class VotingTopic {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Size(min = 5)
    String topicName;
    String linkFull;
    String linkFullInfo;
    String forLink;
    String status; // not active, active, closed
    Integer yes;
    Integer no;

    @Override
    public String toString() {
        return
                "Topic Name='" + topicName + '\'' +
                ", status='" + status + '\'' +
                ", yes=" + yes +
                ", no=" + no +
                '}';
    }
}
