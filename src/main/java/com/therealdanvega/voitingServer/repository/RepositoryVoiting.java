package com.therealdanvega.voitingServer.repository;


import com.therealdanvega.voitingServer.domain.VotingTopic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "topic", path = "topic")
public interface RepositoryVoiting extends JpaRepository<VotingTopic, Long> {

    VotingTopic getVotingTopicByForLinkIs(String topic);

}
