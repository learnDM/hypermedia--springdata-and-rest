package com.therealdanvega.voitingServer.service;

import com.therealdanvega.voitingServer.domain.VotingTopic;
import java.util.List;


public interface ServiceTopic {

    String saveTopic(VotingTopic votingTopic);
    List<VotingTopic> getListTopics();
    void updateTopic(String voiting, String status);

}
