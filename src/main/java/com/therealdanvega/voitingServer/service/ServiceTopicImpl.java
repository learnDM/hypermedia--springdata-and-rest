package com.therealdanvega.voitingServer.service;


import com.therealdanvega.voitingServer.domain.VotingTopic;
import com.therealdanvega.voitingServer.repository.RepositoryVoiting;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import java.util.List;

@Log
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Service
public class ServiceTopicImpl implements ServiceTopic {

    RepositoryVoiting repositoryVoiting;


    @Override
    public String saveTopic(VotingTopic topic) {
        repositoryVoiting.save(topic);
        return "Your topic is saved";
    }

    @Override
    public List<VotingTopic> getListTopics() {
        return repositoryVoiting.findAll();
    }

    @Override
    public void updateTopic(String voiting, String status) {
        log.info("dm-> voiting: " + voiting + ", status: " + status);

        VotingTopic votingTopic = repositoryVoiting.getVotingTopicByForLinkIs(voiting);
        log.info("dm-> votingTopic: " + votingTopic);
        votingTopic.setStatus(status);
        repositoryVoiting.save(votingTopic);
    }

}
