package com.therealdanvega.voitingServer.controller;


import com.therealdanvega.voitingServer.domain.VotingTopic;
import com.therealdanvega.voitingServer.service.ServiceTopic;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.java.Log;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@Log
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
@Controller
public class ControllerVoiting {

     ServiceTopic serviceTopic;

    @GetMapping(value = "/admin_voiting")
    public ModelAndView admin_voiting () {
        ModelAndView mv = new ModelAndView("voiting");

        mv.addObject("topic", new VotingTopic());
        mv.addObject("topics",  serviceTopic.getListTopics());
//        mv.addObject("msg", "Your message:");
        return mv;
    }


    @PostMapping(value = "/admin_voiting")
    public ModelAndView addTopic (@Valid @ModelAttribute VotingTopic topic, BindingResult result) {
        ModelAndView mv = new ModelAndView("voiting");


        if (result.hasErrors()) { // do validation
            mv.addObject("msg", "Some parameters short length!!");
            mv.addObject("topic", topic);
            log.info("Some parameters short length!!");
            return mv;
        }

        topic.setLinkFull("http://localhost:8080/social_poll/"+topic.getTopicName().replaceAll(" ", "-").replaceAll("\\?", ""));
        topic.setLinkFullInfo("http://localhost:8080/inf/social_poll/"+topic.getTopicName().replaceAll(" ", "-").replaceAll("\\?", ""));
        topic.setForLink(topic.getTopicName().replaceAll(" ", "-").replaceAll("\\?", ""));
        topic.setStatus("created");
        topic.setYes(0);
        topic.setNo(0);
        String answer = serviceTopic.saveTopic(topic);

        mv.addObject("topic", new VotingTopic());
        mv.addObject("topics",  serviceTopic.getListTopics());
        mv.addObject("msg", answer);

        return mv;
    }



    @GetMapping(value = "/admin_voiting/{voiting}/changestatus/{status}")
//    @GetMapping(value = "/social_poll/{voiting}/changestatus/{status}")
    public String getListContacts (@PathVariable String voiting, @PathVariable String status  ) {
        serviceTopic.updateTopic(voiting, status);
        return "redirect:/admin_voiting";
    }



}
