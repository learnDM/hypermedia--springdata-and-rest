package com.therealdanvega.voitingClient.service;


import com.therealdanvega.voitingServer.domain.VotingTopic;
import com.therealdanvega.voitingServer.repository.RepositoryVoiting;
import com.therealdanvega.voitingServer.service.ServiceTopic;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Log
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Service
public class ServiceTopicClientImpl implements ServiceTopicClient {

    RestTemplate restTemplate;


    @Override
    public VotingTopic getVotingTopicByForLinkIs(String voiting) {

        String url = "http://localhost:8080/topic/search/getVotingTopicByForLinkIs?topic="+voiting;
        log.info("dm-> getTT() url: " + url);

        VotingTopic topic = restTemplate.getForObject(url, VotingTopic.class);
        log.info("dm-> getTT() topic: " + topic);

        return topic;
    }



    @Override
    public void changeTopic(VotingTopic topic) {
        restTemplate.put("http://localhost:8080/topic/"+topic.getId(), topic);
    }


}
