package com.therealdanvega.voitingClient.service;


import com.therealdanvega.voitingServer.domain.VotingTopic;

import java.util.List;

public interface ServiceTopicClient {

    VotingTopic getVotingTopicByForLinkIs(String voiting);
    void changeTopic(VotingTopic topic);

}
