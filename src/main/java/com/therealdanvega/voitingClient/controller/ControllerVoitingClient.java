package com.therealdanvega.voitingClient.controller;


import com.therealdanvega.voitingClient.service.ServiceTopicClient;
import com.therealdanvega.voitingServer.domain.VotingTopic;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.java.Log;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Log
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
@Controller
public class ControllerVoitingClient {

    ServiceTopicClient serviceTopicClient;


    @GetMapping(value = "/social_poll/{voiting}")
    public ModelAndView getListContacts (@PathVariable String voiting) {

        ModelAndView mv = new ModelAndView("voiting_user");

        VotingTopic topic = serviceTopicClient.getVotingTopicByForLinkIs(voiting);
        log.info("dm -> topic" + topic);

        mv.addObject("topic", topic);
        return mv;
    }



    @ResponseBody
    @GetMapping(value = "/social_poll/{voiting}/{yesno}")
    public String getTT (@PathVariable String voiting, @PathVariable String yesno) {

        VotingTopic topic = serviceTopicClient.getVotingTopicByForLinkIs(voiting);

        if(yesno.equals("yes")) topic.setYes(topic.getYes()+1);
        if(yesno.equals("no")) topic.setNo(topic.getNo()+1);

        serviceTopicClient.changeTopic(topic);

        return "Thanks for you Answer!";
    }



    @ResponseBody
    @GetMapping(value = "/inf/social_poll/{voiting}")
    public String getInf (@PathVariable String voiting) {
        VotingTopic topic = serviceTopicClient.getVotingTopicByForLinkIs(voiting);
        return topic.toString();
    }



}
